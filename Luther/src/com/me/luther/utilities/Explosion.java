package com.me.luther.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.Assets;
import com.me.luther.models.Drawable;
import com.me.luther.models.GameObject;

public class Explosion extends GameObject implements Drawable {

	private float stateTime ;
	public boolean done;

	public Explosion(Vector2 position, float width, float height) {
		super(position, width, height);
		done = false;
	}

	@Override
	public void draw(SpriteBatch batch) {

		if(!done)
		batch.draw(Assets.exp.getKeyFrame(stateTime += Gdx.graphics.getDeltaTime()), this.position.x, this.position.y, this.width,this.height);
		if(Assets.exp.isAnimationFinished(stateTime))
			done = true;
	}

}
