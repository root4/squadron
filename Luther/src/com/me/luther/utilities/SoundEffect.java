package com.me.luther.utilities;

import com.badlogic.gdx.audio.Sound;
import com.me.luther.Assets;


public class SoundEffect {
	public static enum Effect{HIT,DESTROYED, SHIELD,GUN,HURT};

	public static Sound effect;
	
	public SoundEffect(Effect e)
	{
		switch(e)
		{
		case HIT: effect = Assets.noise; break;
		case DESTROYED: effect = Assets.destroy; break;
		case SHIELD: effect = Assets.upShield; break;
		case GUN: effect = Assets.gun; break;
		case HURT: effect = Assets.hurt; break;
		default: effect = Assets.noise; break;
		}

		if(e.equals(Effect.HIT))
			effect.play(.25f);
		else
			effect.play(.75f);
	}
	
}
