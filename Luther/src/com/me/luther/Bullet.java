package com.me.luther;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.me.luther.models.DynamicGameObject;

public class Bullet extends DynamicGameObject implements Poolable{

	public float life = 3; 
	public float angle;
	public float id;
	
	public Bullet(Vector2 position, float width, float height) {
		super(position, width, height);
		angle = 0;
	}

	public void draw(SpriteBatch batch)
	{
		
			this.update();
			
			this.life -=Gdx.graphics.getDeltaTime();
			
		if(this.id == 1)
			batch.draw(Assets.bullet, this.getPosition().x,this.getPosition().y, 0, 0, this.getWidth(), this.getHeight(), 1, 1,this.velocity.angle() - 90);
		else
			batch.draw(Assets.ebullet, this.getPosition().x,this.getPosition().y, 0, 0, Assets.ebullet.getRegionWidth()/3,Assets.ebullet.getRegionHeight()/3, 1,1,0);

			
	}
	public void update()
	{
		this.position.y += this.velocity.y; 
		this.position.x += this.velocity.x;
		
	}

	@Override
	public void reset() {
		this.position.x =0;
		this.position.y= 0;
		this.velocity.set(0,0);
		life = 3;
		
	}
}
