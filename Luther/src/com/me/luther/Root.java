package com.me.luther;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Rectangle;
import com.me.luther.screens.BackgroundScreen;
import com.me.luther.screens.LevelScreen;
import com.me.luther.screens.MenuScreen;
import com.me.luther.screens.ShipScreen;
import com.me.luther.screens.ThemeScreen;

public class Root extends Game
{
	/*This is the entry point for the application
	 * All screen are created here to be used throughout the application lifecycle*/

	public static GameManager mgr;
	public static float height;
	public static float width;
	public Rectangle bounds;
	
	/*SCREENS*/
	
	public ThemeScreen themeScreen;
	public ShipScreen shipScreen;
	public BackgroundScreen backgroundScreen;
	public GameScreen gameScreen;
	public MenuScreen menuScreen;
	public LevelScreen levelScreen;

	
	public void create() {
		bounds = new Rectangle(30,75,430,550);
		height = 640;
		width = 480;
		mgr = new GameManager();
		Assets.load();
		themeScreen = new ThemeScreen(this);
		backgroundScreen = new BackgroundScreen(this);
		shipScreen = new ShipScreen(this);
		levelScreen = new LevelScreen(this);
		gameScreen = new GameScreen(this);
		menuScreen = new MenuScreen(this);
		
	this.setScreen(menuScreen);
	}
	
}