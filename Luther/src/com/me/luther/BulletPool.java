package com.me.luther;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

public class BulletPool extends Pool<Bullet> {

	@Override
	protected Bullet newObject() {
		return new Bullet(new Vector2(0,0), 4,8);
	}

}
