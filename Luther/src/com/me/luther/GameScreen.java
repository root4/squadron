package com.me.luther;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.me.luther.controller.Player;
import com.me.luther.controller.World;
import com.me.luther.models.Ship;
import com.me.luther.screens.GameOverScreen;
import com.me.luther.screens.HUD;

public class GameScreen implements Screen{

	public enum GAMESTATE { RUNNING, GAMEOVER, COMPLETE, PAUSED};
	
	public GAMESTATE state;
	SpriteBatch batch;
	OrthographicCamera cam;
	Input input;
	World world;
	HUD hud;
	Root game;
	int level;
	Rectangle pauseBounds, exitBounds;
	
	
	public GameScreen(Root game) {
		this.game = game;
		pauseBounds = new Rectangle(Root.width/2 -Assets.font.getBounds("Continue").width/2,Root.height/2,Assets.font.getBounds("Continue").width,Assets.font.getBounds("Continue").height);
		exitBounds = new Rectangle(Root.width/2 -Assets.font.getBounds("Exit").width/2,Root.height/2.5f,Assets.font.getBounds("Exit").width,Assets.font.getBounds("Exit").height);

		
	}
	public void update()
	{
		switch(state)
		{
		case COMPLETE: updateComplete();
			break;
		case GAMEOVER: updateOver();
			break;
		case PAUSED: updatePaused();
			break;
		case RUNNING: updateRunning();
			break;
		default:
			break;
		
		}
		
	}
	private void updateRunning() {

		world.update(this);
		this.moveCam();
		cam.update();
		input.getInput(cam);
		hud.getShield(world.getPlayer());
		if(world.getPlayer().getLifes() == 0)
		{
			state = GAMESTATE.GAMEOVER;
			Assets.music.stop();
		}
		
		
	}
	private void updatePaused() {
		Vector3 touch = new Vector3(Gdx.input.getX(), Gdx.input.getY() ,0);
		cam.unproject(touch);
		if(pauseBounds.contains(new Vector2(touch.x,touch.y))&& Gdx.input.isTouched())
		{
			Assets.music.resume();
			state = GAMESTATE.RUNNING;
		}
		if(exitBounds.contains(new Vector2(touch.x,touch.y))&& Gdx.input.isTouched())
		{
			this.level = 0;
			game.setScreen(game.menuScreen);
			Assets.music.stop();
			
		}

	}
	private void updateOver() {
		Root.mgr.setHighScore(world.getPlayer().getScore());
		game.setScreen(new GameOverScreen(game,world.getPlayer().getScore()));		
	}
	private void updateComplete() {
		
	
			this.level++;
			Root.mgr.setCurrentLevel(level);
	
			world.setLevel(Root.mgr.getLevel(level));
			world.reset();
			this.state = GAMESTATE.RUNNING;
		
		
	}
	public void draw()
	{
		switch(state)
		{
		case COMPLETE: drawComplete();
			break;
		case GAMEOVER:drawOver();
			break;
		case PAUSED: drawPaused();
			break;
		case RUNNING:drawRunning();
			break;
		default:
			break;
		
		}
		
	}
	
	
	
	private void drawRunning() {
		Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.begin();
		world.draw(batch);
	
		
		if(hud.draw(batch, world.getPlayer(),state))
		{
			state = GAMESTATE.PAUSED;
			Assets.music.pause();
		}
		batch.end();		
	}
	private void drawPaused() {

		Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.begin();
	
		Assets.font.draw(batch, "Continue", pauseBounds.x , pauseBounds.y+pauseBounds.height);
		Assets.font.draw(batch, "Exit", exitBounds.x , exitBounds.y+exitBounds.height);
		

		
		batch.end();
	}
	private void drawOver() {
		
	}
	private void drawComplete() {
		
		//Gdx.gl.glClearColor(0,0,0, 1);
		//Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		//LevelCompleteScreen.draw(batch);
	}
	@Override
	public void render(float delta) {
		
		
		this.update();
		this.draw();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
		state = GAMESTATE.RUNNING;

		batch = new SpriteBatch();	
		hud = new HUD();
		

		cam = new OrthographicCamera();
		cam.setToOrtho(false, 480,640);
			
		cam.position.set(240,320, 0);
		batch.setProjectionMatrix(cam.combined);
			
		input = new Input();
		world = new World(new Player(new Ship(new Vector2(240,360),39,39)));
		
			Root.mgr.setCurrentLevel(level);
		world.setLevel(Root.mgr.getLevel(level));
		input.setShip(this,game);
	
		Gdx.input.setInputProcessor(input);
		Assets.music.loop();
	}

	@Override
	public void hide() {
		dispose();		
	}

	public void moveCam()
	{
		
		float camX = cam.position.x;
		Vector3 pos = new Vector3(world.getShip().getPosition().x,world.getShip().getPosition().y,0);
		pos.x = -camX+ pos.x;
		if(camX + pos.x -Gdx.graphics.getWidth()/2 >0  && camX + pos.x + Gdx.graphics.getWidth()/2 < 480)
		{
			cam.position.set(world.getShip().getPosition().x,320, 1);
		}

	}
	@Override
	public void pause() {	
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		batch.dispose();	
		
	}

}
