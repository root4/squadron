package com.me.luther;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.controller.Player;
import com.me.luther.models.Drawable;
import com.me.luther.models.GameObject;

public class ShieldUpgrade extends GameObject implements Drawable {

	public ShieldUpgrade(Vector2 position, float width, float height) {
		
		super(position, width, height);
		if(position.y < 100)
			position.y = 100;
	}

	@Override
	public void draw(SpriteBatch batch) {
		
		batch.draw(Assets.shieldUpgrade, this.position.x, this.position.y, this.width,this.height);
	}
	
	public void upgradeShield(Player p1)
	{
		p1.getShip().hitCounter = 0;
	}

}

