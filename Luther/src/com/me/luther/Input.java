package com.me.luther;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.me.luther.models.Ship;

public class Input implements InputProcessor {

	Ship ship;
	float fireTimer = .65f;
	Root game;
	GameScreen screen;
	
	public void setShip(GameScreen screen, Root game)
	{
	this.ship = screen.world.getShip();
	this.game = game;
	this.screen = screen;
	}
	@Override
	public boolean keyDown(int keycode) {
		
		
		return false;
	}
	public void getInput(OrthographicCamera camera)
	{
		
		if(Gdx.input.isKeyPressed(Keys.UP)&&ship.canMoveUp)
			ship.setYVelocity(10);
		if(Gdx.input.isKeyPressed(Keys.DOWN)&&ship.canMoveDown)
			ship.setYVelocity(-10);
		if(Gdx.input.isKeyPressed(Keys.RIGHT)&&ship.canMoveRight)
			ship.setXVelocity(10);
		if(Gdx.input.isKeyPressed(Keys.LEFT)&&ship.canMoveLeft)
			ship.setXVelocity(-10);
	
		
		Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(touchPos);
	
			float xDist = touchPos.x - ship.getPosition().x;
			
			float yDist = touchPos.y +50- ship.getPosition().y;
			float dist = (float) Math.sqrt(xDist * xDist + yDist* yDist);  // distance formula 
			if(dist >1)
			{
				ship.getPosition().x += xDist * .15f;
				ship.getPosition().y += yDist *.15f;
			}
			if(Gdx.input.isTouched())
			{
			fireTimer -= Gdx.graphics.getDeltaTime();
			if(fireTimer<=0)
			{
				ship.fireGun(null, screen.world);
				fireTimer = .15f;
			}
			}
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
}
