package com.me.luther;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class GameManager {

	
	
	private static final String PREFS_NAME = "luthergame";
	
	
	public GameManager() {

		/*Places the key values if this is the first insance of the game*/
		
		if(getPrefs().getBoolean("INIT") == false)
		{
			setBackground("data/backgrounds/purple.png");
			setShip("white");
			getPrefs().putBoolean("INIT",true); 
			

		}

	
	}

	protected Preferences getPrefs() {
		return Gdx.app.getPreferences(PREFS_NAME);
	}
	public float[] getHighScore()
	{
		

		float[] ret = new float[5];
		for(int i = 0; i <5; i++)
			ret[i] = getPrefs().getFloat(Integer.toString(i));
	
		return ret;
	}
	
	
	public void setHighScore(float score)
	{
		Preferences prefs = getPrefs();

		loop :for(int i = 0; i<5; i++)
				if(score > this.getHighScore()[i])
				{
					for(int j = 5; j >i; j--)
						prefs.putFloat(Integer.toString(j), this.getHighScore()[j-1]);
					
					prefs.putFloat(Integer.toString(i), score);

					break loop;
				}
		prefs.flush();
	}


	public String getLevel(int level) {
		switch(level)
		{
			case 0: return "one"; 
			case 1: return "two";
			case 2: return "three"; 
			case 3: return "four"; 
			case 4: return "five";
		}
		return "five";
	}

	public int getCurrentLevel() {
		return getPrefs().getInteger("CURRENTLEVEL");
	}
	public void setCurrentLevel(int level)
	{
		Preferences prefs = getPrefs();
		
		prefs.putInteger("CURRENTLEVEL", level);
		prefs.flush();
	}
	public void setBackground(String path)
	{
		Preferences prefs = getPrefs();
		
		prefs.putString("BACKGROUNDPATH", path);
		prefs.flush();
	}
	public void setShip(String name)
	{
		
		Preferences prefs = getPrefs();
		
		prefs.putString("CURRENTSHIP", name);
		prefs.flush();
	}
	public String getBackground()
	{
		return getPrefs().getString("BACKGROUNDPATH");
	}
	public String getShip()
	{
		return getPrefs().getString("CURRENTSHIP");
	}
	
	
}
