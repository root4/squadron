package com.me.luther;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.models.Drawable;
import com.me.luther.models.GameObject;
import com.me.luther.models.Ship;

public class GunUpgrade extends GameObject implements Drawable {

	public GunUpgrade(Vector2 position, float width, float height) {
		super(position, width, height);
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(Assets.upgrade, this.position.x, this.position.y,this.width,this.height);
	}
	public void upgradeShip(Ship ship)
	{
		if(ship.getGunNum()<5)
			ship.setGunNum(ship.getGunNum() + 1);
	}

}
