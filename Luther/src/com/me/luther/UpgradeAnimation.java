package com.me.luther;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.models.Drawable;
import com.me.luther.models.DynamicGameObject;

public class UpgradeAnimation extends DynamicGameObject implements Drawable{

	
	float timer;
	public boolean isDone;
	Color color;
	String text;
	public UpgradeAnimation(Vector2 position, float width, float height, int i) {
		super(position, width, height);
		this.velocity.y = 3;
		timer = 1f;
		isDone = false;
		if( i == 0)
		{
			color = new Color(Color.CYAN);
			text= "Sheild";
		}
		if(i ==1)
		{
			color = new Color(Color.YELLOW);
			text = "Guns +1";
		}
		if(i ==2)
		{
			color = new Color(Color.YELLOW);
			text = "";
		}
		if(i ==3)
		{
			color = new Color(Color.CYAN);
			text = "";
		}
		
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.setColor(color);
		color.a = timer/1f;
			
		batch.draw(Assets.bump, position.x, position.y,width,height);
		Assets.small.setColor(color);
		Assets.small.draw(batch, text, position.x , position.y);
		position.add(velocity);
		timer -= Gdx.graphics.getDeltaTime();
		if(timer <=0)
			isDone = true;
		batch.setColor(Color.WHITE);
		Assets.small.setColor(Color.WHITE);
	}
	
	

}
