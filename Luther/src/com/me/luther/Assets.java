package com.me.luther;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

	public static TextureRegion bump;
	public static TextureRegion shieldUpgrade;
	public static TextureRegion bullet;
	public static TextureRegion ebullet;
	public static TextureRegion playerLife;
	public static TextureRegion motherShip;
	public static TextureRegion ship;
	public static TextureRegion enemy;
	public static Texture background;
	public static TextureRegion upgrade;
	public static TextureRegion up;
	public static TextureRegion down;
	public static BitmapFont font;
	public static BitmapFont header;
	public static BitmapFont game;
	public static BitmapFont small;
	public static TextureRegion back;
	public static TextureRegion ufo;
	public static Animation hit;
	public static Animation exp;
	public static Texture explosion;
	public static Animation shield;
	public static TextureRegion shieldBar;
	public static TextureRegion shieldFill;
	public static Animation grace;
	public static Texture spriteSheet;
	public static Random rn;
	public static Sound noise;
	public static Sound destroy;
	public static Sound gun;
	public static Sound upShield;
	public static Sound hurt;
	public static Sound music;

	public static void load() {

		noise = Gdx.audio.newSound(Gdx.files.internal("data/Audio/hit.wav"));
		destroy = Gdx.audio.newSound(Gdx.files
				.internal("data/Audio/destroy.wav"));
		gun = Gdx.audio.newSound(Gdx.files.internal("data/Audio/gun.mp3"));
		upShield = Gdx.audio.newSound(Gdx.files
				.internal("data/Audio/shield.mp3"));
		hurt = Gdx.audio.newSound(Gdx.files.internal("data/Audio/hurt.wav"));
		music = Gdx.audio.newSound(Gdx.files.internal("data/Audio/music.ogg"));

		rn = new Random();
		spriteSheet = new Texture(Gdx.files.internal("data/sheet2.png"));
		bump = new TextureRegion(spriteSheet, 32, 0, 32, 32);
		shieldUpgrade = new TextureRegion(spriteSheet, 0, 72, 34, 33);
		motherShip = new TextureRegion(spriteSheet, 139, 72, 82, 84);
		playerLife = new TextureRegion(spriteSheet, 224, 32, 32, 24);
		shieldBar = new TextureRegion(spriteSheet, 0, 33, 222, 39);
		shieldFill = new TextureRegion(spriteSheet, 224, 0, 16, 32);
		back = new TextureRegion(spriteSheet, 0, 0, 30, 33);

		if (Root.mgr.getShip().equals("white"))
			ship = new TextureRegion(spriteSheet, 258, 0, 99, 75);
		else if (Root.mgr.getShip().equals("red"))
			ship = new TextureRegion(spriteSheet, 0, 181, 99, 75);
		else if (Root.mgr.getShip().equals("green"))
			ship = new TextureRegion(spriteSheet, 101, 181, 99, 75);
		else if (Root.mgr.getShip().equals("blue"))
			ship = new TextureRegion(spriteSheet, 359, 0, 99, 75);
		else
			ship = new TextureRegion(spriteSheet, 258, 0, 99, 75);

		ufo = new TextureRegion(spriteSheet, 256, 96, 91, 91);
		bullet = new TextureRegion(spriteSheet, 240, 24, 4, 8);
		ebullet = new TextureRegion(spriteSheet, 240, 0, 16, 16);

		enemy = new TextureRegion(spriteSheet, 33, 72, 104, 84);
		upgrade = new TextureRegion(spriteSheet, 192, 0, 32, 32);
		up = new TextureRegion(spriteSheet, 128, 0, 64, 32);
		down = new TextureRegion(spriteSheet, 64, 0, 64, 32);

		game = new BitmapFont(Gdx.files.internal("data/font.fnt"),
				Gdx.files.internal("data/font.png"), false);
		small = new BitmapFont(Gdx.files.internal("data/font.fnt"),
				Gdx.files.internal("data/font.png"), false);
		small.scale(-.5f);
		explosion = new Texture(Gdx.files.internal("data/exp2.png"));
		font = new BitmapFont(Gdx.files.internal("data/new.fnt"));
		header = new BitmapFont(Gdx.files.internal("data/new.fnt"));
		header.setScale(1.5f);
		background = new Texture(Gdx.files.internal(Root.mgr.getBackground()));
		background.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);

		TextureRegion[] tex = new TextureRegion[3];
		tex[0] = new TextureRegion(new Texture(
				Gdx.files.internal("data/Ships/d1.png")));
		tex[1] = new TextureRegion(new Texture(
				Gdx.files.internal("data/Ships/d2.png")));

		tex[2] = new TextureRegion(new Texture(
				Gdx.files.internal("data/Ships/d3.png")));
		hit = new Animation(.1f, tex);
		hit.setPlayMode(Animation.LOOP_PINGPONG);

		TextureRegion[] expl = new TextureRegion[10];
		int index = 0;
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 5; j++) {
				expl[index] = new TextureRegion(explosion, j * 92, i * 92, 91,
						91);
				index++;
			}

		exp = new Animation(.1f, expl);

		TextureRegion[] sh = new TextureRegion[3];
		sh[0] = new TextureRegion(new Texture(
				Gdx.files.internal("data/shield1.png")));
		sh[1] = new TextureRegion(new Texture(
				Gdx.files.internal("data/shield2.png")));
		sh[2] = new TextureRegion(new Texture(
				Gdx.files.internal("data/shield3.png")));
		shield = new Animation(.1f, sh);

		TextureRegion[] gr = new TextureRegion[2];
		gr[0] = new TextureRegion(ship);
		gr[1] = new TextureRegion(ship, 0, 0, 0, 0);
		grace = new Animation(.25f, gr);
		grace.setPlayMode(Animation.LOOP);

	}

	public static void dispose() {
		noise.dispose();
		music.dispose();
		hurt.dispose();
		gun.dispose();
		destroy.dispose();
		upShield.dispose();
		background.dispose();
		explosion.dispose();
		font.dispose();
		header.dispose();
		spriteSheet.dispose();

	}
}
