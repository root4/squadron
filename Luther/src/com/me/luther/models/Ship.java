package com.me.luther.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.me.luther.Assets;
import com.me.luther.Bullet;
import com.me.luther.BulletPool;
import com.me.luther.controller.World;

public class Ship extends ShipModel implements Drawable {

	public boolean canMoveUp, canMoveDown, canMoveRight, canMoveLeft;
	protected float distanceTravelled;
	private float stateTime;
	private boolean isHit;

	public Ship(Vector2 position, float width, float height) {
		super(position, width, height);
		stateTime = 0;
		isHit = false;
		canMoveUp = true;
		canMoveDown = true;
		canMoveLeft = true;
		canMoveRight = true;
		distanceTravelled = 0;
		this.MAXHITS = 3;
	}

	public void draw(SpriteBatch batch) {
		batch.draw(Assets.ship, this.position.x, this.position.y,
				this.getWidth(), this.getHeight());
		if (hitCounter > 0)
			batch.draw(Assets.hit.getKeyFrames()[hitCounter - 1],
					this.position.x, this.position.y, this.getWidth(),
					this.getHeight());
		distanceTravelled += 10 * Gdx.graphics.getDeltaTime();
		if (isHit) {
			stateTime += Gdx.graphics.getDeltaTime();
			batch.draw(Assets.shield.getKeyFrame(stateTime, false),
					this.position.x - this.width / 9, this.position.y,
					this.width * 1.3f, this.height * 1.2f);
			if (Assets.shield.isAnimationFinished(stateTime)) {
				stateTime = 0;
				isHit = false;
			}
		}

	}

	public void update(World world) {
		this.position.x = this.position.x + this.velocity.x;
		this.position.y = this.position.y + this.velocity.y;
		this.velocity.set(0, 0);

	}

	public void hit() {
		if (hitCounter < MAXHITS)
			hitCounter++;
		isHit = true;
	}

	public void fireGun(Vector2 tar, World world) {

		switch (getGunNum()) {
		case 1:
			fireOne(world.bullets, world.bulletPool);
			break;
		case 2:
			fireTwo(world.bullets, world.bulletPool);
			break;
		case 3:
			fireThree(world.bullets, world.bulletPool);
			break;
		case 4:
			fireFour(world.bullets, world.bulletPool);
			break;
		case 5:
			fireFive(world.bullets, world.bulletPool);
			break;
		}
	}

	private void fireFive(Array<Bullet> bullets, BulletPool bulletPool) {
		for (int i = -2; i < 3; i++) {

			Bullet bul = bulletPool.obtain();
			bul.position.x = this.position.x + this.width / 2 - 2 + i * 16;
			bul.position.y = this.position.y;
			bul.width = 4;
			bul.height = 8;
			bul.velocity.y = 10;
			bul.id = 1;

			bullets.add(bul);

		}
	}

	private void fireFour(Array<Bullet> bullets, BulletPool bulletPool) {
		for (int i = -2; i < 3; i++) {
			if (i != 0) {
				Bullet bul = bulletPool.obtain();
				bul.position.x = this.position.x + this.width / 2 - 2 + i * 16;
				bul.position.y = this.position.y;
				bul.width = 4;
				bul.height = 8;
				bul.velocity.y = 10;
				bul.id = 1;
				bullets.add(bul);
			}
		}
	}

	private void fireThree(Array<Bullet> bullets, BulletPool bulletPool) {
		for (int i = -1; i < 2; i++) {

			Bullet bul = bulletPool.obtain();
			bul.position.x = this.position.x + this.width / 2 - 2 + i * 16;
			bul.position.y = this.position.y;
			bul.width = 4;
			bul.height = 8;
			bul.velocity.y = 10;
			bul.id = 1;

			bullets.add(bul);

		}
	}

	private void fireTwo(Array<Bullet> bullets, BulletPool bulletPool) {
		for (int i = -1; i < 2; i++) {
			if (i != 0) {
				Bullet bul = bulletPool.obtain();
				bul.position.x = this.position.x + this.width / 2 - 2 + i * 8;
				bul.position.y = this.position.y;
				bul.width = 4;
				bul.height = 8;
				bul.velocity.y = 10;
				bul.id = 1;

				bullets.add(bul);
			}
		}

	}

	private void fireOne(Array<Bullet> bullets, BulletPool bulletPool) {
		Bullet bul = bulletPool.obtain();
		bul.getPosition().x = this.position.x + this.width / 2 - 2;
		bul.getPosition().y = this.position.y;
		bul.setWidth(4);
		bul.setHeight(8);
		bul.velocity.y = 10;
		bul.id = 1;

		bullets.add(bul);
	}

	public float getDistanceTravelled() {
		return distanceTravelled;
	}

	public void setDistanceTravelled(float distanceTravelled) {
		this.distanceTravelled = distanceTravelled;
	}

}
