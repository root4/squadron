package com.me.luther.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.Assets;
import com.me.luther.controller.Path;
import com.me.luther.controller.World;

public class Enemy extends ShipModel {

	public Path path;
	public int id;
	public float chaseTime;
	public float fireTimer;
	public int squadID;

	public Enemy(Vector2 position, float width, float height, int pathIndex,
			int squadID) {
		super(position, width, height);
		path = new Path();
		path.getPath(pathIndex);
		MAXHITS = 2;
		this.squadID = squadID;
		this.bulletSpeed = 5;
		fireTimer = 1 + rn.nextFloat();
		this.position.x += path.pop().getX();
		this.position.x += path.stagger.x * squadID;
		this.position.y += path.stagger.y * squadID;
		this.MAXHITS = 3;
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(Assets.enemy, this.getPosition().x, this.getPosition().y,
				this.getWidth() / 2, this.getHeight() / 2, this.getWidth(),
				this.getHeight(), 1, 1, this.getVelocity().angle() + 90);

	}

	public void hit() {
		hitCounter++;
	}

	public void update(World world) {
		if (path.isEmpty()) {
			this.setXVelocity(0);
			this.setYVelocity(-1f);
			this.position.add(this.velocity);
		} else {
			if (path.index == 4)
				this.follow(new Vector2(this.getPosition().x + 5, this
						.getPosition().y - 10));
			if (path.index == 5)
				this.follow(new Vector2(this.getPosition().x - 5, this
						.getPosition().y - 10));

		}

		fireTimer -= Gdx.graphics.getDeltaTime();
		if (fireTimer < 0) {
			fireTimer = 1 + rn.nextFloat();
			this.fire(world.getShip().getPosition(), world.bullets,
					world.bulletPool);
		}

	}

}
