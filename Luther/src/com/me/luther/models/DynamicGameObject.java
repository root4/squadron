package com.me.luther.models;

import com.badlogic.gdx.math.Vector2;

public class DynamicGameObject extends GameObject {

	protected Vector2 velocity;

	public DynamicGameObject(Vector2 position, float width, float height) {
		super(position, width, height);
		this.velocity = new Vector2(0, 0);
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public void setXVelocity(float x) {
		this.velocity.x = x;
	}

	public void setYVelocity(float y) {
		this.velocity.y = y;
	}

	public void follow(Vector2 tar) {

		float tx = tar.x - position.x;
		float ty = tar.y - position.y;
		float dist = (float) Math.sqrt(tx * tx + ty * ty);

		if (dist > 10) {

			velocity.lerp(new Vector2(tx / dist * 3, ty / dist * 3), .1f);

		}
		position.x += velocity.x;
		position.y += velocity.y;
	}

}
