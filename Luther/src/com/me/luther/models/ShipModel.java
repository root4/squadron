package com.me.luther.models;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.me.luther.Bullet;
import com.me.luther.BulletPool;
import com.me.luther.controller.World;

public abstract class ShipModel extends DynamicGameObject{
	
	
	private int gunNum = 1;
	int bulletSpeed = 3;
	Vector2 target;
	public int MAXHITS;
	Random rn;
	public int hitCounter;
	
	public abstract  void update(World world);
	public abstract void draw(SpriteBatch batch);

	
	public ShipModel(Vector2 position, float width, float height) {
		super(position, width, height);
		
		target = new Vector2(0,0);
		 rn  = new Random();
	}
	
	public void fire(Vector2 tar, Array<Bullet> bullets, BulletPool bulletPool)
	{
		if(tar == null)
		{
			Bullet bul = bulletPool.obtain();
			bul.getPosition().x = this.position.x +this.width/2 - 2;
			bul.getPosition().y = this.position.y ;
			bul.setWidth(4); bul.setHeight(8);
			bul.velocity.y = 10;
			bul.id = 0;
			bullets.add(bul);
		}
		else
		{
			target = tar.cpy();
		
			target.x  = rn.nextInt((int) (((target.x+100) - (target.x - 100))+1))+(target.x -100);
			Bullet bul = bulletPool.obtain();
			bul.getPosition().x = this.position.x +this.width/2 - 2;
			bul.getPosition().y = this.position.y ;
			bul.angle = (float) Math.atan2(target.y - bul.getPosition().y, target.x - bul.getPosition().x);
			bul.angle = (float) (bul.angle *(180/Math.PI));
			
			bul.setWidth(4); bul.setHeight(8);
			bul.velocity.x = (float) (Math.cos(bul.angle * Math.PI / 180) * bulletSpeed);
			bul.velocity.y = (float) (Math.sin(bul.angle  * Math.PI / 180) * bulletSpeed);
			bul.id = 0;
			

			bullets.add(bul);
		}
	}

	public int getGunNum() {
		return gunNum;
	}

	public void setGunNum(int gunNum) {
		this.gunNum = gunNum;
	}
	public abstract void hit();
	
}
