package com.me.luther.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.me.luther.Assets;
import com.me.luther.Bullet;
import com.me.luther.BulletPool;
import com.me.luther.Root;
import com.me.luther.controller.World;

public class MotherShip extends ShipModel implements Drawable {

	
	public float fireTimer = .25f;
	public MotherShip(Vector2 position, float width, float height) {
		super(position, width, height);
		this.MAXHITS = 50 * (1+Root.mgr.getCurrentLevel());
		this.setGunNum(Root.mgr.getCurrentLevel());
		
	}

	
	public void update(World world)
	{
		this.follow(new Vector2(240- this.width/2,400));
		fireTimer -=Gdx.graphics.getDeltaTime();
		if(fireTimer<=0)
		{
			fireTimer = .25f;
			this.fire(world.getShip().getPosition(), world.bullets,world.bulletPool);
			switch(getGunNum())
			{
			case 1: fireOne(world.bullets,world.bulletPool, world.getShip().getPosition()); break;
			case 2: fireTwo(world.bullets,world.bulletPool, world.getShip().getPosition()); break;
			case 3: fireThree(world.bullets,world.bulletPool, world.getShip().getPosition()); break;
			case 4: fireFour(world.bullets,world.bulletPool, world.getShip().getPosition()); break;
			case 5: fireFive(world.bullets,world.bulletPool,world.getShip().getPosition()); break;
			default: fireFive(world.bullets,world.bulletPool,world.getShip().getPosition()); break;
			}
		}
	}


	private void fireFive(Array<Bullet> bullets, BulletPool bulletPool, Vector2 tar) {
		for (int i =-2; i <3; i++)
		{
			
			target = tar.cpy();
			
			target.x  = rn.nextInt((int) (((target.x+150) - (target.x - 150))+1))+(target.x -150);
			Bullet bul = bulletPool.obtain();
			bul.getPosition().x = this.position.x +this.width/2 - 2;
			bul.getPosition().y = this.position.y ;
			bul.angle = (float) Math.atan2(target.y - bul.getPosition().y, target.x - bul.getPosition().x);
			bul.angle = (float) (bul.angle *(180/Math.PI));
			
			bul.setWidth(4); bul.setHeight(8);
			bul.velocity.x = (float) (Math.cos(bul.angle * Math.PI / 180) * bulletSpeed);
			bul.velocity.y = (float) (Math.sin(bul.angle  * Math.PI / 180) * bulletSpeed);
			bul.id = 0;

			bullets.add(bul);
		}		
	}

	private void fireFour(Array<Bullet> bullets, BulletPool bulletPool, Vector2 tar) {
		for (int i =-2; i <3; i++)
		{
			if(i!=0)
			{
				target = tar.cpy();
				
				target.x  = rn.nextInt((int) (((target.x+100) - (target.x - 100))+1))+(target.x -100);
				Bullet bul = bulletPool.obtain();
				bul.getPosition().x = this.position.x +this.width/2 - 2;
				bul.getPosition().y = this.position.y ;
				bul.angle = (float) Math.atan2(target.y - bul.getPosition().y, target.x - bul.getPosition().x);
				bul.angle = (float) (bul.angle *(180/Math.PI));
				
				bul.setWidth(4); bul.setHeight(8);
				bul.velocity.x = (float) (Math.cos(bul.angle * Math.PI / 180) * bulletSpeed);
				bul.velocity.y = (float) (Math.sin(bul.angle  * Math.PI / 180) * bulletSpeed);
				bul.id = 0;

				bullets.add(bul);
			}
		}		
	}

	private void fireThree(Array<Bullet> bullets, BulletPool bulletPool,Vector2 tar) {
		for (int i =-1; i <2; i++)
		{
			
			
			target = tar.cpy();
			
			target.x  = rn.nextInt((int) (((target.x+100) - (target.x - 100))+1))+(target.x -100);
			Bullet bul = bulletPool.obtain();
			bul.getPosition().x = this.position.x +this.width/2 - 2;
			bul.getPosition().y = this.position.y ;
			bul.angle = (float) Math.atan2(target.y - bul.getPosition().y, target.x - bul.getPosition().x);
			bul.angle = (float) (bul.angle *(180/Math.PI));
			
			bul.setWidth(4); bul.setHeight(8);
			bul.velocity.x = (float) (Math.cos(bul.angle * Math.PI / 180) * bulletSpeed);
			bul.velocity.y = (float) (Math.sin(bul.angle  * Math.PI / 180) * bulletSpeed);
			bul.id = 0;

			bullets.add(bul);
			
		}	
	}

	private void fireTwo(Array<Bullet> bullets, BulletPool bulletPool, Vector2 tar) {
		for (int i =-1; i <2; i++)
		{
			if(i!=0)
			{
				target = tar.cpy();
				
				target.x  = rn.nextInt((int) (((target.x+100) - (target.x - 100))+1))+(target.x -100);
				Bullet bul = bulletPool.obtain();
				bul.getPosition().x = this.position.x +this.width/2 - 2;
				bul.getPosition().y = this.position.y ;
				bul.angle = (float) Math.atan2(target.y - bul.getPosition().y, target.x - bul.getPosition().x);
				bul.angle = (float) (bul.angle *(180/Math.PI));
				
				bul.setWidth(4); bul.setHeight(8);
				bul.velocity.x = (float) (Math.cos(bul.angle * Math.PI / 180) * bulletSpeed);
				bul.velocity.y = (float) (Math.sin(bul.angle  * Math.PI / 180) * bulletSpeed);
				bul.id = 0;

				bullets.add(bul);
			}
		}
		
	}
	private void fireOne(Array<Bullet> bullets, BulletPool bulletPool, Vector2 tar){
		target = tar.cpy();
		
		target.x  = rn.nextInt((int) (((target.x+100) - (target.x - 100))+1))+(target.x -100);
		Bullet bul = bulletPool.obtain();
		bul.getPosition().x = this.position.x +this.width/2 - 2;
		bul.getPosition().y = this.position.y ;
		bul.angle = (float) Math.atan2(target.y - bul.getPosition().y, target.x - bul.getPosition().x);
		bul.angle = (float) (bul.angle *(180/Math.PI));
		
		bul.setWidth(4); bul.setHeight(8);
		bul.velocity.x = (float) (Math.cos(bul.angle * Math.PI / 180) * bulletSpeed);
		bul.velocity.y = (float) (Math.sin(bul.angle  * Math.PI / 180) * bulletSpeed);
		bul.id = 0;
		

		bullets.add(bul);
	}

	



	@Override
	public void draw(SpriteBatch batch) {
		
		batch.draw(Assets.motherShip, this.position.x,this.position.y);
	}
	public void hit()
	{
		this.hitCounter ++;
		
		
	}
	
}
