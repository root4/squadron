package com.me.luther.models;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Drawable {

	public void draw(SpriteBatch batch);

}
