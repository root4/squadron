package com.me.luther.models;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.Assets;
import com.me.luther.controller.Path;
import com.me.luther.controller.World;

public class Ufo extends ShipModel {

	public Path path;
	public int id;

	public float bulletSize = 8;
	public int squadID;

	public float rotation;

	public Ufo(Vector2 position, float width, float height, int pathIndex,
			int squadID) {
		super(position, width, height);
		this.squadID = squadID;
		rotation = 0;
		path = new Path();
		path.getPath(pathIndex);
		MAXHITS = 2;
		this.position.x += path.pop().getX();
		this.position.x += path.stagger.x * squadID;
		this.position.y += path.stagger.y * squadID;
	}

	public void draw(SpriteBatch batch) {
		batch.draw(Assets.ufo, this.getPosition().x, this.getPosition().y,
				this.getWidth() / 2, this.getHeight() / 2, this.getWidth(),
				this.getHeight(), 1, 1, rotation += 3);
		if (rotation > 360)
			rotation = 0;

	}

	public void hit() {
		hitCounter++;
	}

	public void update(World world) {
		if (!path.isEmpty()) {
			this.follow(path.getNext().getPos());

			if (this.position.dst(path.getNext().getPos()) < 10) {
				path.pop();
				this.fire(world.getShip().getPosition(), world.bullets,
						world.bulletPool);
			}

		} else {
			this.setXVelocity(0);
			this.setYVelocity(-.5f);
			this.position.add(this.velocity);
		}

	}

}
