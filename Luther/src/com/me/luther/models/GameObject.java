package com.me.luther.models;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class GameObject {

	protected Vector2 position; 
	protected float width;
	protected float height;
	
	public GameObject(Vector2 position, float width,float height)
	{
		this.position = position;
		this.width = width;
		this.height = height;
	}
	
	public Vector2 getPosition() {
		return position;
	}
	public void setPosition(Vector2 position) {
		this.position = position;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public Rectangle getBounds()
	{
		return new Rectangle(position.x,position.y,width,height);
	}
	public void setBounds(Rectangle bounds)
	{
		this.position.x = bounds.x;
		this.position.y = bounds.y;
		this.width = bounds.width;
		this.height = bounds.height;
	}
}
