package com.me.luther.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.me.luther.Assets;
import com.me.luther.Root;

public class LevelScreen implements Screen {

	Stage stage;
	Skin skin;
	Table table;
	Label one, two, three, four, five;
	ImageButton back;
	Label label, highscore;
	Label title;
	private Root game;
	SpriteBatch batch;
	Sprite b1, b2;
	float width, height;
	boolean newScore;
	int index;
	Color color;
	float interval = .05f;
	Image arrow;

	public LevelScreen(Root game) {
		this.game = game;
		newScore = false;
		index = 5;
		color = new Color(1, 1, 1, 1);
	}

	public void newHighScore(float score) {
		loop: for (int i = 0; i < 5; i++) {
			if (score == Root.mgr.getHighScore()[i]) {
				newScore = true;
				index = i;
				break loop;
			}
		}
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(.1f, .1f, .1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.begin();

		if (b1.getY() + b1.getHeight() <= 0)
			b1.setY(b2.getY() + b2.getHeight());
		if (b2.getY() + b2.getHeight() <= 0)
			b2.setY(b1.getY() + b1.getHeight());
		b1.setPosition(b1.getX(), b1.getY() - 5);
		b2.setPosition(b2.getX(), b2.getY() - 5);
		b1.draw(batch);
		b2.draw(batch);
		highscore.setColor(color);
		batch.end();
		stage.act();
		stage.draw();
		if (color.b >= 1 || color.b <= 0)
			interval = -interval;

		color.b += interval;
		arrow.setColor(color);

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {

		this.width = Root.width;
		this.height = Root.height;
		skin = new Skin();
		stage = new Stage();
		batch = (SpriteBatch) stage.getSpriteBatch();
		table = new Table();
		table.setFillParent(true);
		b1 = new Sprite(Assets.background);

		b1.setPosition(0, 0);
		b1.setSize(width, height);
		b2 = new Sprite(b1);
		b2.setY(height);

		arrow = new Image(new TextureRegion(new Texture(
				Gdx.files.internal("data/arrow.png"))));
		TextButtonStyle tbstyle = new TextButtonStyle();
		tbstyle.up = new TextureRegionDrawable(new TextureRegion(Assets.up));
		tbstyle.down = new TextureRegionDrawable(new TextureRegion(Assets.down));
		tbstyle.font = Assets.font;
		skin.add("default", tbstyle);

		LabelStyle lstyle = new LabelStyle();
		lstyle.font = Assets.header;
		skin.add("default", lstyle);
		LabelStyle b = new LabelStyle();
		b.font = Assets.font;
		skin.add("back", b);
		LabelStyle small = new LabelStyle();
		small.font = Assets.small;
		skin.add("small", small);
		highscore = new Label("HIGHSCORE!", skin.get("back", LabelStyle.class));
		highscore.setX(150);
		highscore.setY(height / 1.45f);
		label = new Label("Back", skin.get("back", LabelStyle.class));
		label.setX(60);
		label.setY(height - label.getHeight() * 1.5f);
		back = new ImageButton(new TextureRegionDrawable(new TextureRegion(
				Assets.back)));
		back.setWidth(64);
		back.setHeight(64);
		back.setY(height - back.getHeight());

		title = new Label("High Scores", skin);

		one = new Label("1.  " + Root.mgr.getHighScore()[0], skin.get("small",
				LabelStyle.class));
		two = new Label("2.  " + Root.mgr.getHighScore()[1], skin.get("small",
				LabelStyle.class));
		three = new Label("3.  " + Root.mgr.getHighScore()[2], skin.get(
				"small", LabelStyle.class));
		four = new Label("4.  " + Root.mgr.getHighScore()[3], skin.get("small",
				LabelStyle.class));
		five = new Label("5.  " + Root.mgr.getHighScore()[4], skin.get("small",
				LabelStyle.class));

		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.menuScreen);
			}
		});
		label.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.menuScreen);
			}
		});

		table.add(title).row();
		table.add(one).width(150).space(15).row();
		table.add(two).width(150).space(15).row();
		table.add(three).width(150).space(15).row();
		table.add(four).width(150).space(15).row();
		table.add(five).width(150).space(15);
		stage.addActor(back);
		stage.addActor(label);
		stage.addActor(table);
		if (newScore) {
			switch (index) {
			case 0:
				arrow.setY(340);
				break;
			case 1:
				arrow.setY(310);
				break;
			case 2:
				arrow.setY(279);
				break;
			case 3:
				arrow.setY(248);
				break;
			case 4:
				arrow.setY(215);
				break;
			}

			arrow.setX(width / 3.5f);
			stage.addActor(highscore);

			stage.addActor(arrow);
		}

		Gdx.input.setInputProcessor(stage);
		Camera cam = new OrthographicCamera(width, height);
		cam.position.set(width / 2, height / 2, 0);
		stage.setCamera(cam);
		stage.setViewport(width, height);
		table.setBounds(0, 0, width, height);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
