package com.me.luther.screens;

import java.text.DecimalFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.me.luther.Assets;
import com.me.luther.GameScreen.GAMESTATE;
import com.me.luther.Root;
import com.me.luther.controller.Player;

public  class HUD {

	float percent = 296;
	DecimalFormat df ;
	Rectangle pauseBounds ;
	public HUD()
	{
		df = new DecimalFormat();
		df.setMaximumFractionDigits(0);
	}
	
	public boolean draw(SpriteBatch batch, Player p1, GAMESTATE state) {
		Matrix4 hudUi = new Matrix4();
		hudUi.setToOrtho2D(0,0,480,640);                
		batch.setProjectionMatrix(hudUi);
		batch.draw(Assets.shieldBar,10,  10, 300,50);
		batch.draw(Assets.shieldFill, 13, 12,percent,47);
		
		Assets.game.draw(batch, "Shield", 95, 45);
		for(int i = 0; i < p1.getLifes() ; i++)
			batch.draw(Assets.playerLife, 340 + (i * 50), 10,32,33);
		
		Assets.small.draw(batch, "Score: " + df.format(p1.getScore()), 200, 630);
		Assets.small.draw(batch, "High Score: " + df.format(Root.mgr.getHighScore()[0]), 10,630);
		
		if(Gdx.input.isTouched(1) || Gdx.input.isButtonPressed(Input.Buttons.RIGHT))
		{
			state = GAMESTATE.PAUSED;
			return true;
		}
		return false;
	}

	public void getShield(Player p1)
	{
		percent = (float)p1.getShip().hitCounter/(float)p1.getShip().MAXHITS;
		percent = 1-percent;
		percent *= 296;
		p1.setShield((int) percent);
			
		
	}
	
}
