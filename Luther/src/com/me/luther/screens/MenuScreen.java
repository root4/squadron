package com.me.luther.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.me.luther.Assets;
import com.me.luther.Root;

public class MenuScreen implements Screen {

	Stage stage;
	Skin skin;
	Table table;
	TextButton play, level, theme, exit;
	Label title;
	private Root game;
	SpriteBatch batch;
	Sprite b1, b2;
	float width, height;

	public MenuScreen(Root game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(.1f, .1f, .1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.begin();

		if (b1.getY() + b1.getHeight() <= 0)
			b1.setY(b2.getY() + b2.getHeight());
		if (b2.getY() + b2.getHeight() <= 0)
			b2.setY(b1.getY() + b1.getHeight());
		b1.setPosition(b1.getX(), b1.getY() - 5);
		b2.setPosition(b2.getX(), b2.getY() - 5);
		b1.draw(batch);
		b2.draw(batch);
		batch.end();
		stage.act();
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		this.width = Root.width;
		this.height = Root.height;
		skin = new Skin();
		stage = new Stage();
		batch = (SpriteBatch) stage.getSpriteBatch();
		table = new Table();
		table.setFillParent(true);
		b1 = new Sprite(Assets.background);

		b1.setPosition(0, 0);
		b1.setSize(width, height);
		b2 = new Sprite(b1);
		b2.setY(height);

		TextButtonStyle tbstyle = new TextButtonStyle();
		tbstyle.up = new TextureRegionDrawable(new TextureRegion(Assets.up));
		tbstyle.down = new TextureRegionDrawable(new TextureRegion(Assets.down));
		tbstyle.font = Assets.font;
		skin.add("default", tbstyle);

		LabelStyle lstyle = new LabelStyle();
		lstyle.font = Assets.header;
		skin.add("default", lstyle);
		LabelStyle small = new LabelStyle();
		small.font = Assets.small;
		skin.add("small", small);

		title = new Label("Squadron", skin);

		play = new TextButton("Play", skin);
		level = new TextButton("Scores", skin);
		theme = new TextButton("Theme", skin);
		exit = new TextButton("Exit", skin);
		play.pad(15, 50, 15, 50);
		level.pad(15, 25, 15, 25);
		theme.pad(15, 50, 15, 50);
		exit.pad(15, 50, 15, 50);
		play.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(game.gameScreen);
			}
		});
		level.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.levelScreen.newScore = false;
				game.setScreen(game.levelScreen);
			}
		});
		theme.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new ThemeScreen(game));
			}
		});
		exit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Assets.dispose();
				Gdx.app.exit();
			}
		});

		table.add(title).row();
		table.add(play).width(150).space(15).row();
		table.add(level).width(150).space(15).row();
		table.add(theme).width(150).space(15).row();
		table.add(exit).width(150).space(15);

		stage.addActor(table);

		Gdx.input.setInputProcessor(stage);
		Camera cam = new OrthographicCamera(width, height);
		cam.position.set(width / 2, height / 2, 0);
		stage.setCamera(cam);
		stage.setViewport(width, height);
		table.setBounds(0, 0, width, height);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
