package com.me.luther.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.me.luther.Assets;
import com.me.luther.Root;

public class ThemeScreen implements Screen {

	Root game;
	Stage stage;
	Skin skin;
	Table table;
	Sprite b1, b2;
	float width, height;
	TextButton backButton, shipButton;
	Label header, label;
	ImageButton back;

	public ThemeScreen(Root game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {

		stage.getSpriteBatch().begin();
		if (b1.getY() + b1.getHeight() <= 0)
			b1.setY(b2.getY() + b2.getHeight());
		if (b2.getY() + b2.getHeight() <= 0)
			b2.setY(b1.getY() + b1.getHeight());
		b1.setPosition(b1.getX(), b1.getY() - 5);
		b2.setPosition(b2.getX(), b2.getY() - 5);
		b1.draw(stage.getSpriteBatch());
		b2.draw(stage.getSpriteBatch());
		stage.getSpriteBatch().end();
		stage.act();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		this.width = Root.width;
		this.height = Root.height;
		stage = new Stage();
		table = new Table();
		table.setFillParent(true);
		skin = new Skin();

		TextButtonStyle tbstyle = new TextButtonStyle();
		tbstyle.up = new TextureRegionDrawable(new TextureRegion(Assets.up));
		tbstyle.down = new TextureRegionDrawable(new TextureRegion(Assets.down));
		tbstyle.font = Assets.font;
		skin.add("default", tbstyle);

		LabelStyle lstyle = new LabelStyle();
		lstyle.font = Assets.header;
		skin.add("default", lstyle);

		LabelStyle b = new LabelStyle();
		b.font = Assets.font;
		skin.add("back", b);
		label = new Label("Back", skin.get("back", LabelStyle.class));
		label.setX(60);
		label.setY(height - label.getHeight() * 1.5f);

		backButton = new TextButton("Change\r\nBackground", skin);
		shipButton = new TextButton("Change\r\nShip", skin);

		header = new Label("Choose Theme", skin);
		back = new ImageButton(new TextureRegionDrawable(new TextureRegion(
				Assets.back)));
		back.setWidth(64);
		back.setHeight(64);
		back.setY(height - back.getHeight());

		backButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new BackgroundScreen(game));
			}
		});
		shipButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new ShipScreen(game));
			}
		});

		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MenuScreen(game));
			}
		});
		label.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MenuScreen(game));
			}
		});

		backButton.pad(15, 50, 15, 50);
		shipButton.pad(15, 50, 15, 50);

		table.add(header).top().row();
		table.add(backButton).width(250).space(15).row();
		table.add(shipButton).width(250).space(15).row();

		b1 = new Sprite(Assets.background);

		b1.setPosition(0, 0);
		b1.setSize(width, height);
		b2 = new Sprite(b1);
		b2.setY(height);

		Camera cam = new OrthographicCamera(width, height);
		cam.position.set(width / 2, height / 2, 0);
		stage.setCamera(cam);
		stage.setViewport(width, height);
		table.setBounds(0, 0, width, height);

		stage.addActor(table);
		stage.addActor(back);
		stage.addActor(label);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
