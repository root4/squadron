package com.me.luther.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.me.luther.Assets;
import com.me.luther.models.Drawable;
import com.me.luther.models.Ship;

public class Player implements Drawable {
	
	protected Ship ship;
	protected float score;
	protected int lifes;
	protected int shield;
	protected float graceTime;
	protected boolean alive;
	protected float stateTime;
	
	public Player(Ship ship)
	{
		this.ship = ship;
		this.score = 0;
		this.lifes = 3;
		this.shield = 3;
		alive = true;
		graceTime = 2;
	}
	@Override
	public void draw(SpriteBatch batch) {
		if(alive)
			this.ship.draw(batch);
		else
		{
			graceTime -= Gdx.graphics.getDeltaTime();
			stateTime += Gdx.graphics.getDeltaTime();
			batch.draw(Assets.grace.getKeyFrame(stateTime), ship.getPosition().x, ship.getPosition().y,ship.getWidth(),ship.getHeight());
			if(graceTime <=0)
			{
				alive = true;
				graceTime =2;
			}
		}
	}
	
	public void reset()
	{
		alive = false;
		ship.setPosition(new Vector2(240,320));
		ship.hitCounter = 0;
		this.ship.setGunNum(1);

	}
	public void update(World world)
	{
		this.ship.update(world);
	}
	public int getShield()
	{
		return shield;
	}
	public void setShield(int shield)
	{
		this.shield = shield;
	}
	public void addScore()
	{
		score++;
	}
	public void subLife()
	{
		lifes--;
	}
	public void subShield()
	{
		shield--;
	}
	public Ship getShip() {
		return ship;
	}
	public float getScore() {
		return score;
	}
	public int getLifes() {
		return lifes;
	}
	

}
