package com.me.luther.controller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.me.luther.models.Enemy;
import com.me.luther.models.MotherShip;
import com.me.luther.models.ShipModel;
import com.me.luther.models.Ufo;

public class Squadron {
	

	Array<ShipModel> ships;
	Path path;
	float startDist;
	
	public Squadron(float size,int p, float startDist)
	{
	
		ships = new Array<ShipModel>();
		for(int i = 0; i< size; i++)
		{
			int plus = 50;
			if(p ==1)
				plus = -50;
			
			if(p ==3 || p==4 || p==5 || p==7 || p==8)
			{
				ships.add(new Enemy(new Vector2(plus, 700),40,40,p,i));
				
			}
			else if(p==6)
			{
				ships.add(new MotherShip(new Vector2(240,700), 100,100));
			}
			else
			{
				ships.add(new Ufo(new Vector2(plus, 700),32,32,p,i));

			}
			
		}
		
		
		
		this.startDist = startDist;

	}
	
	public void update(float shipDistance,  World world)
	{
		if(this.startDist <= shipDistance)
			for(ShipModel ship: ships)
			{
				ship.update(world);
				if(ship.getPosition().y<20)
					ships.removeValue(ship, true);
			}
			
	}
	public void draw(SpriteBatch batch)
	{
		for(ShipModel ship: ships)
			ship.draw(batch);
	}

}
