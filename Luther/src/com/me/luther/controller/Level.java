package com.me.luther.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Level {

	Array<Squadron> sq;
	int highScore;
	String dir = "data/levels/";

	public Level(String level)
	{
	
		FileHandle file = Gdx.files.internal(dir+level+".TXT");
		String st = new String();
		st = file.readString();
		String[] s = st.split("\\r?\\n");
		
		sq= new Array<Squadron>();
		for(int i = 0; i< s.length; i++)
		{
			String[] temp = s[i].split(",");
			
			sq.add( new Squadron(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), Integer.parseInt(temp[2])));
	
		}
		
	}
	
	public void update(float shipDistance, Vector2 ship, World world)
	{
		for(Squadron q: sq)
		{
			
			q.update(shipDistance, world);
		}
	}
	public void draw(SpriteBatch batch)
	{
		for(Squadron q: sq)
			q.draw(batch);
	}
}
