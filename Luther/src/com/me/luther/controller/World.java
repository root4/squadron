package com.me.luther.controller;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.me.luther.Assets;
import com.me.luther.Bullet;
import com.me.luther.BulletPool;
import com.me.luther.GameScreen;
import com.me.luther.GameScreen.GAMESTATE;
import com.me.luther.GunUpgrade;
import com.me.luther.ShieldUpgrade;
import com.me.luther.UpgradeAnimation;
import com.me.luther.models.MotherShip;
import com.me.luther.models.Ship;
import com.me.luther.models.ShipModel;
import com.me.luther.utilities.Explosion;
import com.me.luther.utilities.SoundEffect;
import com.me.luther.utilities.SoundEffect.Effect;

public class World {

	Rectangle bounds;
	Ship ship;
	Array<Explosion> exp;
	Level l;
	Sprite b1, b2;
	Array<GunUpgrade> gunUpgrades;
	Array<ShieldUpgrade> shieldUpgrades;
	Array<UpgradeAnimation> upAnim;
	ShapeRenderer shape;
	Player p1;
	Random rn;
	public Array<Bullet> bullets;
	public Array<SoundEffect> sounds;
	public BulletPool bulletPool;
	float statetime;
	String levelDir;

	public World(Player p1) {
		shape = new ShapeRenderer();
		bounds = new Rectangle(10, 60, 440, 600);

		bullets = new Array<Bullet>();
		sounds = new Array<SoundEffect>();
		bulletPool = new BulletPool();
		exp = new Array<Explosion>();
		upAnim = new Array<UpgradeAnimation>();
		this.p1 = p1;
		this.ship = p1.ship;
		rn = new Random();

		gunUpgrades = new Array<GunUpgrade>();
		shieldUpgrades = new Array<ShieldUpgrade>();
		b1 = new Sprite(Assets.background);

		b1.setPosition(0, 0);
		b1.setSize(480, 640);
		b2 = new Sprite(b1);
		b2.setY(640);
	}

	public void setLevel(String levelDir) {

		this.levelDir = levelDir;
		l = new Level(levelDir);

	}

	public Ship getShip() {
		return ship;
	}

	public void cd(GameScreen stage) {

		hits: for (Bullet b : bullets) {
			if (b.getBounds().overlaps(ship.getBounds()) && b.id == 0
					&& p1.alive) {
				ship.hit();
				sounds.add(new SoundEffect(Effect.HURT));
				if (p1.shield == 0) {
					p1.subLife();
					p1.reset();
				}
				exp.add(new Explosion(b.getPosition(), 16, 16));
				bullets.removeValue(b, true);
				break hits;

			}
			if (b.life <= 0 || b.getPosition().y > 650) {
				bullets.removeValue(b, true);
				bulletPool.free(b);
			}
			for (Squadron e : l.sq) {
				for (ShipModel t : e.ships)
					if (b.getBounds().overlaps(t.getBounds()) && b.id == 1) {
						p1.addScore();
						sounds.add(new SoundEffect(Effect.HIT));
						if (p1.getScore() % 200 == 1 && p1.getScore() != 1)
							shieldUpgrades.add(new ShieldUpgrade(new Vector2(rn
									.nextInt(430), rn.nextInt(600)), 32, 32));

						t.hit();
						bullets.removeValue(b, true);
						if (p1.getScore() % 100 == 1 && p1.getScore() != 1) {
							gunUpgrades.add(new GunUpgrade(new Vector2(rn
									.nextInt(430), rn.nextInt(600)), 32, 32));
						}
						if (t.hitCounter >= t.MAXHITS) {

							if (t.getClass() == MotherShip.class) {
								sounds.add(new SoundEffect(Effect.DESTROYED));

								stage.state = GAMESTATE.COMPLETE;

								exp.add(new Explosion(t.getPosition(), t
										.getWidth(), t.getHeight()));
								e.ships.removeValue(t, true);

								break hits;
							}
							exp.add(new Explosion(t.getPosition(),
									t.getWidth(), t.getHeight()));
							e.ships.removeValue(t, true);
						}

						exp.add(new Explosion(b.getPosition(), 16, 16));
					}

			}
		}

		for (GunUpgrade u : gunUpgrades)
			if (u.getBounds().overlaps(ship.getBounds())) {
				if (p1.getShip().getGunNum() == 5)
					upAnim.add(new UpgradeAnimation(
							new Vector2(u.getPosition()), 32, 32, 2));
				else
					upAnim.add(new UpgradeAnimation(
							new Vector2(u.getPosition()), 32, 32, 1));

				p1.score += 10;
				sounds.add(new SoundEffect(Effect.GUN));

				u.upgradeShip(ship);
				gunUpgrades.removeValue(u, true);
			}
		for (ShieldUpgrade sh : shieldUpgrades)
			if (sh.getBounds().overlaps(ship.getBounds())) {
				if (p1.getShield() == 296)
					upAnim.add(new UpgradeAnimation(new Vector2(sh
							.getPosition()), 32, 32, 3));
				else
					upAnim.add(new UpgradeAnimation(new Vector2(sh
							.getPosition()), 32, 32, 0));

				sounds.add(new SoundEffect(Effect.SHIELD));

				p1.score += 10;
				sh.upgradeShield(p1);
				shieldUpgrades.removeValue(sh, true);
			}

	}

	public void draw(SpriteBatch batch) {

		b1.draw(batch);
		b2.draw(batch);

		l.draw(batch);

		for (GunUpgrade u : gunUpgrades)
			u.draw(batch);
		for (ShieldUpgrade sh : shieldUpgrades)
			sh.draw(batch);
		for (Bullet b : bullets)
			b.draw(batch);
		for (UpgradeAnimation up : upAnim) {
			up.draw(batch);
			if (up.isDone)
				upAnim.removeValue(up, true);
		}

		for (Explosion g : exp) {
			g.draw(batch);
			if (g.done)
				exp.removeValue(g, true);

		}
		p1.draw(batch);

	}

	public void update(GameScreen stage) {

		if (b1.getY() + b1.getHeight() <= 0)
			b1.setY(b2.getY() + b2.getHeight());
		if (b2.getY() + b2.getHeight() <= 0)
			b2.setY(b1.getY() + b1.getHeight());

		b1.setPosition(b1.getX(), b1.getY() - 5);
		b2.setPosition(b2.getX(), b2.getY() - 5);
		l.update(ship.getDistanceTravelled(), ship.getPosition(), this);
		p1.update(this);
		this.cd(stage);

	}

	public Player getPlayer() {
		return p1;
	}

	public void reset() {
		bullets = new Array<Bullet>();
		sounds = new Array<SoundEffect>();
		bulletPool = new BulletPool();
		gunUpgrades = new Array<GunUpgrade>();
		shieldUpgrades = new Array<ShieldUpgrade>();
		p1.getShip().setDistanceTravelled(0);

	}

}
