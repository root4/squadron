package com.me.luther.controller;

import java.util.Stack;

import com.badlogic.gdx.math.Vector2;

public class Path {

	public Stack<Node> nodes;
	public Vector2 stagger;
	public int index;
	public class Node{
		
		public Vector2 node;
		public boolean shootPoint;
	
		
		public Node(Vector2 pos, boolean shoot)
		{
		this.node = pos;
		this.shootPoint = shoot;
		}
		
		public float getX(){ return node.x;}
		public float getY(){ return node.y;}
		public boolean canShoot(){ return shootPoint;}
		public Vector2 getPos(){ return node;}
		
	}
	public boolean isEmpty()
	{
		return nodes.isEmpty();
	}
	
	public  Node getNext()
	{
		return nodes.peek();
	}
	public Node pop()
	{
		return nodes.pop();
	}
	public void getPath(int index)
	{
		this.index = index;
		switch(index)
		{
		
		case 1: this.getOne(); break;
		case 2: this.getTwo(); break;
		case 3: this.getThree(); break;
		case 4: this.getFour(); break;
		case 5: this.getFive(); break;
		case 6: this.getSix(); break;
		case 7:	this.getSeven(); break;
		case 8: this.getEight(); break;
		case 9: this.getNine(); break;
		case 0: this.getZero(); break;
		
		}
	}

	private void getZero() {
		// TODO Auto-generated method stub
		
	}

	private void getNine() {
		// TODO Auto-generated method stub
		
	}

	private void getEight() {
		nodes = new Stack<Node>();
		nodes.push(new Node(new Vector2(390,0),false));
		stagger = new Vector2(-50,0);		
	}

	private void getSeven() {
		nodes = new Stack<Node>();
		nodes.push(new Node(new Vector2(10,0),false));
		stagger = new Vector2(50,0);		
	}

	private void getSix() {
		 nodes = new Stack<Node>();
		 nodes.push(new Node(new Vector2(0,0),false));		
	}

	private void getOne() {

		 nodes = new Stack<Node>();
		 nodes.push(new Node(new Vector2(0,0),false));
		 nodes.push(new Node(new Vector2(250,450),true));
		 nodes.push(new Node(new Vector2(50,550),true));
		 nodes.push(new Node(new Vector2(50,300),true));
		 nodes.push(new Node(new Vector2(250,450),true));
		 nodes.push(new Node (new Vector2(-75,0), false));
		 
		 stagger = new Vector2(-100,0);
		
		
	}
	private void getTwo()
	{
		nodes = new Stack<Node>();
		nodes.push(new Node(new Vector2(500,0),false));
		 nodes.push(new Node(new Vector2(250,450),true));
		 nodes.push(new Node(new Vector2(430,550),true));
		 nodes.push(new Node(new Vector2(430,300),true));
		nodes.push(new Node(new Vector2(250,450),true));
		nodes.push(new Node(new Vector2(480,0),false));
		stagger = new Vector2(100,0);
		
	}
	private void getThree()
	{
		nodes = new Stack<Node>();
		nodes.push(new Node(new Vector2(150,0),false));
		stagger = new Vector2(50,0);
	}
	private void getFour()
	{
		nodes = new Stack<Node>();
		nodes.push(new Node(new Vector2(0,0),false));
		nodes.push(new Node(new Vector2(0,0),false));

		stagger = new Vector2(50,50);

	}
	private void getFive()
	{
		nodes = new Stack<Node>();
		nodes.push(new Node(new Vector2(0,0),false));
		nodes.push(new Node(new Vector2(480,0),false));
		

		stagger = new Vector2(-50,50);
	}
	
}
